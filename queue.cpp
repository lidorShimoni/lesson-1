#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->arr = new int[size];
	q->index = 0;
	q->size = size;
}

void cleanQueue(queue* q)
{
	q->arr = 0;
	q->size = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->index < q->size)
	{
		q->arr[q->index] = newValue;
		q->index++;
	}
}

int dequeue(queue* q)
{
	int tmp = 0;
	if (q->index > 0)
	{
		tmp = q->arr[0];
		for (int i = 0; i < (q->index - 1); i++)
		{
			q->arr[i] = q->arr[i + 1];
		}
		q->index--;
		return tmp;
	}
	return -1;
}